import React, {Component} from 'react';
import {List} from 'antd';
import { getMultiPosts } from '../Services/RedditService';
import InfiniteScroll from 'react-infinite-scroller';
import './MusicList.css';
import Post from './Post';

const NUM_LOADING_POSTS = 26;

class MusicList extends Component {

    state = {
        posts: [],
        loading: true,
        before: null,
        after: null
    };

    async componentWillMount() {
        await this.handleInfiniteOnLoad();
    }

    handleInfiniteOnLoad = async () => {

        this.setState((state) => {
            return {
                loading: true,
                posts: state.posts.concat(new Array(NUM_LOADING_POSTS))
            };
        });

        const {posts, after, before} = await getMultiPosts(this.props.subreddits, {
            after: this.state.after
        });

        this.setState((state) => {
            return {
                before,
                after,
                posts: state.posts.filter(x => x !== null).concat(posts),
                loading: false
            }
        });
    };

    render() {
        return <div className="music-list-infinite-container">
            <h1>Posts</h1>

            <InfiniteScroll
                loadMore={this.handleInfiniteOnLoad}
                hasMore={!this.state.loading && this.state.after !== null}
                useWindow={false}>

                <List dataSource={this.state.posts}
                      bordered={true}
                      renderItem={item => {
                          if (item) {
                              return <Post data={item.data}/>
                          }
                          else {
                              return <Post loading={true}/>
                          }
                      }}/>
            </InfiniteScroll>
        </div>
    }
}

export default MusicList