import React, { Component } from 'react';
import { List, Skeleton } from 'antd';
import { Avatar } from 'antd';

class Post extends Component {
    render() {
        const { data, loading }= this.props;
        let post;

        if (!loading) {
            post = <>
                <List.Item.Meta
                    avatar={<Avatar size="large" shape="square" src={data.thumbnail}/>}
                    title={<a href={data.url}>{data.title}</a>}
                    description={<p>{data.subreddit} - {data.author} - {data.domain} - {data.created}</p>}
                />

                <div>{data.ups}</div>
            </>
        }
        else {
            post = <Skeleton avatar={{ shape: 'square', size: 'large' }} title={false} loading={loading} active/>
        }

        return <List.Item>
            {post}
        </List.Item>
    }
}

export default Post;