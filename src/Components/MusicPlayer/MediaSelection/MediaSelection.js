import React, { Component } from 'react';
import { List } from 'antd';
import { getMultiPosts } from '../../../Services/RedditService';
import InfiniteScroll from 'react-infinite-scroller';
import './MediaSelection.css';
import Post from '../../Shared/Post/Post';

const NUM_LOADING_POSTS = 26;

class MediaSelection extends Component {

    state = {
        posts: [],
        loading: true,
        before: null,
        after: null,
        initialLoad: true
    };

    async componentWillMount() {
        await this.handleInfiniteOnLoad();

        this.setState({
            initialLoad: false
        });
    }

    handleInfiniteOnLoad = async () => {

        this.setState((state) => {
            return {
                loading: true,
            };
        });

        const { posts, after, before } = await getMultiPosts(this.props.subreddits, {
            after: this.state.after
        });

        this.setState((state) => {
            return {
                before,
                after,
                posts: state.posts.concat(posts),
                loading: false,
            }
        });
    };

    render() {
        const emptyPosts = [];

        for(let ix = 0; ix < NUM_LOADING_POSTS; ix++) {
            emptyPosts.push(
                <Post loading={true} key={ix}/>
            )
        }

        return (
            <div className="music-list">
                <h1>Posts</h1>

                <InfiniteScroll
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={!this.state.loading && this.state.after !== null}
                    useWindow={false}>

                    <List dataSource={this.state.posts}
                          bordered={true}
                          threshold={250}
                          renderItem={item => {
                              return <Post data={item.data}/>
                          }}>

                        {this.state.loading && emptyPosts}

                    </List>
                </InfiniteScroll>
            </div>
        );
    }
}

export default MediaSelection