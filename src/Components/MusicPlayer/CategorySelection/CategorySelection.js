import React, { Component } from 'react';
import './CategorySelection.css';
import Post from '../../Shared/Post/Post';

class CategorySelection extends Component {
    render() {
        return (
            <div className="category-selection">
                <Post loading={true}/>
            </div>
        );
    }
}

export default CategorySelection;