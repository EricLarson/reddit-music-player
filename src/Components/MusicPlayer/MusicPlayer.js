import React, { Component } from 'react';
import MediaSelection from './MediaSelection/MediaSelection';
import './MusicPlayer.css';
import CategorySelection from './CategorySelection/CategorySelection';
import MediaView from './MediaView/MediaView';
import MediaProgress from './MediaProgress/MediaProgress';

class MusicPlayer extends Component {

    render() {

        return (
            <>
                <div className="top-view">

                    <div className="item">
                        <CategorySelection/>
                    </div>
                    <div className="item">

                        <MediaSelection subreddits={["listentothis"]}/>
                    </div>

                    <div className="item">
                        <MediaView/>
                    </div>
                </div>

                <div className="bottom-view">
                    <MediaProgress/>
                </div>
            </>
        );
    }
}

export default MusicPlayer;