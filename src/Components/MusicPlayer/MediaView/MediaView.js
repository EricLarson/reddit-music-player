import React, { Component } from 'react';
import MediaPlayer from './MediaPlayer/MediaPlayer';
import Comments from './Comments/Comments';
import "./MediaView.css";

class MediaView extends Component {
    render() {
        return (
            <div className="media-view">
                <MediaPlayer/>
                <Comments/>
            </div>
        );
    }
}

export default MediaView;