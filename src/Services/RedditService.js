export const sortTypes = {
    hot: 'hot',
    new: 'new',
    controversial: 'controversial',
    top: 'top',
    rising: 'rising'
};

export const sortTimes = {
    hour: 'hour',
    day: 'day',
    week: 'week',
    month: 'month',
    year: 'year',
    all: 'all',
};

const defaultOptions = {
    sortType: sortTypes.hot,
    sortTime: null,
    after: null,
    before: null
};

const timeBasedSorts = [
    sortTypes.hot,
    sortTypes.top,
    sortTypes.controversial
];

export async function getPosts(subreddit, options) {

    if (subreddit === null || typeof subreddit !== 'string' || subreddit.trim().length === 0) {
        throw new Error('Invalid subreddit name');
    }

    return await getRedditPosts([subreddit], {...defaultOptions, ...options});
}

export async function getMultiPosts(subredditList, options = defaultOptions) {

    if (subredditList == null || !Array.isArray(subredditList)) {
        throw new Error('Subreddit list must be an array');
    }

    if (subredditList.length === 0) {
        throw new Error('Subreddit list is empty');
    }

    const subredditString = subredditList.map(x => x.trim()).join('+');

    return await getRedditPosts(subredditString, {...defaultOptions, ...options});
}

async function getRedditPosts(subredditString, options) {

    const isTimeBasedSort = () => {
        return timeBasedSorts.includes(options.sortTime);
    };

    if (sortTypes[options.sortType] === null) {
        throw new Error('Sort type not supported')
    }

    if (options.sortTime !== null && sortTimes[options.sortTime] === null) {
        throw new Error('Sort time not supported');
    }

    if (isTimeBasedSort() && sortTimes[options.sortTime] === null) {
        throw new Error('Sort time no given on time based sort');
    }

    const baseUrl = 'https://www.reddit.com/r/';
    let url = baseUrl + subredditString + '/' + options.sortType + '.json';

    let queries = [];

    if (isTimeBasedSort()) {
        queries.push('t=' + options.sortTime);
    }

    if (options.after) {
        queries.push('after=' + options.after);
    }

    if (queries.length > 0) {
        url += '?' + queries.join('&');
    }

    const response = await fetch(url);
    const redditResponse = await response.json();

    return {
        before: redditResponse.data.before,
        after: redditResponse.data.after,
        posts: redditResponse.data.children
    };
}