import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import MusicPlayer from './Components/MusicPlayer/MusicPlayer';

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={MusicPlayer}/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
